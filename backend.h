#pragma once
#include <QString>

class Backend {
public:
    struct TransferReport {
        int num_files, num_matched, num_moved;
    };

    Backend();
    ~Backend();
    TransferReport transfer_files ();
    QVector<TransferReport> read_statistics_file ();

    QString regex_str, src_dir_path, dst_dir_path;
    int run_interval = 60;
};
