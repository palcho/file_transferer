#pragma once
#include <QWidget>

// forward class declarations
class QPushButton;
class QHBoxLayout;
class QVBoxLayout;
class QLineEdit;
class QSpinBox;
class QFormLayout;
class Backend;
class Graph;
class QProgressBar;

// if you try inheriting from QMainWindow, instead of QWidget, nothing appears on
// the screen when using Layouts and when setting the window as the layout's parent;
// although inheriting from QMainWindow is the default in the QtCreator lol
class MainWindow : public QWidget
{
    Q_OBJECT // required by the Qt meta-class creation

public:
    MainWindow (QWidget *parent = nullptr);
    ~MainWindow ();

signals:
    // todo

public slots:
    void run_button_pressed ();
    void regex_input_changed ();
    void source_input_changed ();
    void destin_input_changed ();
    void interval_selector_changed ();
    void run_timer_alarmed ();
    void bar_timer_alarmed ();

    // Qt requires that any widget that has a parent must be allocated by itself on the heap
    // for its memory management. therefore, all of the data presented below are pointers
private:
    bool app_running = false;
    Backend *backend;
    QTimer *run_timer, *bar_timer;

    // widgets
    QLineEdit *regex_input, *source_input, *destin_input;
    QSpinBox *interval_selector;
    QPushButton *run_button;
    Graph *graph;
    QProgressBar *interval_bar;
};
