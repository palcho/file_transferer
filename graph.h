#pragma once
#include <QtCharts/QChartView>
#include "backend.h"

namespace QtCharts {
    class QLineSeries;
    class QValueAxis;
}

class Graph : public QtCharts::QChartView {
public:
    explicit Graph (QWidget *parent = nullptr);
    ~Graph();

    void update_graph (const Backend::TransferReport *reports, int quantity = 1);

private:
    QtCharts::QLineSeries *num_files_line, *num_matched_line, *num_moved_line;
    QtCharts::QValueAxis *x_axis, *y_axis;
    int current_x = 0, max_y = 0;
};
