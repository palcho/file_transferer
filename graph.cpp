#include "graph.h"
#include <QtCharts/QLineSeries>
#include <QtCharts/QAreaSeries>
#include <QtCharts/QChart>
#include <QtCharts/QValueAxis>
#include <QPoint>

using namespace QtCharts;

Graph::Graph (QWidget *parent) :
    QChartView (parent),
    num_files_line (new QLineSeries),
    num_matched_line (new QLineSeries),
    num_moved_line (new QLineSeries),
    x_axis (new QValueAxis),
    y_axis (new QValueAxis)
{
    // make colored areas from the x-axis to each line
    auto num_files_area = new QAreaSeries(num_files_line);
    auto num_matched_area = new QAreaSeries(num_matched_line);
    auto num_moved_area = new QAreaSeries(num_moved_line);
    auto chart = new QChart;

    // label each series
    num_files_area->setName("All Files");
    num_matched_area->setName("Matched Files");
    num_moved_area->setName("Moved Files");

    // add them to chart
    chart->addSeries(num_files_area);
    chart->addSeries(num_matched_area);
    chart->addSeries(num_moved_area);

    // create axes and attach them all
    chart->addAxis(x_axis, Qt::AlignBottom);
    chart->addAxis(y_axis, Qt::AlignLeft);

    num_files_area->attachAxis(x_axis);
    num_files_area->attachAxis(y_axis);
    num_matched_area->attachAxis(x_axis);
    num_matched_area->attachAxis(y_axis);
    num_moved_area->attachAxis(x_axis);
    num_moved_area->attachAxis(y_axis);

    // initial axes range
    x_axis->setRange(0, 1);
    y_axis->setRange(0, 1);

    chart->setTitle("Files In The Source Directory Per Interval Cycle");

    setRenderHint(QPainter::Antialiasing);
    setChart(chart);
}

void Graph::update_graph (const Backend::TransferReport *reports, int quantity) {
    // update lines from graph
    for (int i = 0; i < quantity; i++) {
        *num_files_line << QPoint(current_x, reports[i].num_files);
        *num_matched_line << QPoint(current_x, reports[i].num_matched);
        *num_moved_line << QPoint(current_x, reports[i].num_moved);
        current_x++;
        if (reports[i].num_files > max_y) {
            max_y = reports[i].num_files;
        }
    }
    // and update its axes
    x_axis->setRange(0, current_x);
    y_axis->setRange(0, max_y + 1);
}

Graph::~Graph () {}
