File Transferer Development Journal and Instructions
==================

## Tuesday - Fist Day

On my first attempt tackling this project, I tried to use QML with C++. All when smoothly when designing the Window, until the last point: the charts. "I should only have to place the ChartView in the hierarchy of Layouts as I have done previously with the other items", I though. Very wrongly, I found out. The process had many back and forths between different QML files just to make it visible, and the subsequent C++ integration would add even more still. [video that explains the process](https://www.youtube.com/watch?v=9BcAYDlpuT8)

I stepped down from all that QML for now, and started working with only C++, using Qt's QWidget API. This is more manageable, since I studied Qt for now only 4 days, so I'm still falling in all its many pitfalls. [video explaining the dozens of ways you can do the same thing in Qt](https://www.youtube.com/watch?v=TOqvODKULhw)

## Thursday - Last Day

It's done.


# How to Compile

Use qmake to generate a makefile and then compile with it. I did it all on Linux. It was not tested on Windows, as I don't have the right tools there, but Qt is multiplataform, so it should work.

### Obs.:

The REGEX used is perl style and an empty string matches anything.

An empty directory name matches the one from which the software is running.

I made a separate file to store statistics, since what is in the log file and what is shown in the graph don't match much.

The repository is not organized in folders because I couldn't tell qmake how to find the files.
