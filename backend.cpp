#include "backend.h"
#include <QDir>
#include <QRegularExpression>
#include <QStringList>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <QRegularExpression>
#include <QVector>

Backend::Backend () {
    // read from configuration file
    QFile config_file ("transfer_config.txt");
    // although human-legible, this file is made for the program to read only
    if (config_file.open(QIODevice::ReadOnly)) {
        QTextStream cin (&config_file);
        regex_str = cin.readLine();
        src_dir_path = cin.readLine();
        dst_dir_path = cin.readLine();
        run_interval = cin.readLine().toInt();
        config_file.close();
    }
}


Backend::TransferReport Backend::transfer_files () {
    QRegExp regex (regex_str);
    QDir src_folder (src_dir_path);

    // get the list of all files in the source directory
    QStringList file_list = src_folder.entryList(QStringList("*"), QDir::Files);
    int num_moved = 0, num_matched = 0;

    // open log file
    QFile log_file ("transfer_log.txt");
    log_file.open(QIODevice::WriteOnly | QIODevice::Append); // @todo: check for failure
    QTextStream clog (&log_file);
    // get current time
    auto time_string = QDateTime::currentDateTime().toString("{ yyyy-MM-dd hh:mm:ss.zzz }");

    // count and move matched files
    for (const QString& file: file_list) {
        if (regex.indexIn(file) != -1) {
            if (QFile::rename(src_dir_path + "/" + file, dst_dir_path + "/" + file)) {
                num_moved++;
                clog << "SUCCESS: ";
            } else {
                clog << "FAILURE: ";
            }
            // write to log file
            clog << time_string << ", src: '" << src_dir_path << "', dst: '" << dst_dir_path
                 << "', file: '" << file << "'\n";
            num_matched++;
        }
    }
    log_file.close();

    TransferReport report = { file_list.count(), num_matched, num_moved };
    // write new report to statistics file
    QFile stat_file ("statistics.txt");
    stat_file.open(QIODevice::WriteOnly | QIODevice::Append); // @todo: check for failure
    QTextStream cstat (&stat_file);
    cstat << report.num_files << " " << report.num_matched  << " "<< report.num_moved << "\n";
    stat_file.close();

    return report;
}

QVector<Backend::TransferReport> Backend::read_statistics_file () {
    QFile stat_file ("statistics.txt");
    QVector<TransferReport> reports;

    // although human-legible, this file is made for the program to read only
    if (stat_file.open(QIODevice::ReadOnly)) {
        QTextStream cin (&stat_file);

        while (!cin.atEnd()) {
            TransferReport tr;
            cin >> tr.num_files >> tr.num_matched >> tr.num_moved;
            reports.push_back(tr);
        }
        stat_file.close();
    }

    return reports;
}


Backend::~Backend() {
    // update configuration file
    QFile config_file ("transfer_config.txt");
    config_file.open(QIODevice::WriteOnly); // @todo: check for failure
    QTextStream cout (&config_file);

    cout << regex_str << "\n" << src_dir_path << "\n" << dst_dir_path << "\n" << run_interval << "\n";

    config_file.close();
}
