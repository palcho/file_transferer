#include "mainwindow.h"
#include "backend.h"
#include "graph.h"
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QSpinBox>
#include <QFormLayout>
#include <QTimer>
#include <QProgressBar>

using namespace QtCharts; // for QtCharts::QChart and QtCharts::QChartView

MainWindow::MainWindow (QWidget *parent) :
    QWidget(parent),
    backend (new Backend),
    run_timer (new QTimer(this)),
    bar_timer (new QTimer(this)),
    // @done: add initial text of LineEdits from saved data
    regex_input (new QLineEdit),
    source_input (new QLineEdit),
    destin_input (new QLineEdit),
    interval_selector (new QSpinBox),
    run_button (new QPushButton("Run App")),
    graph (new Graph),
    interval_bar (new QProgressBar)
{
    setMinimumWidth(500);
    // layout creation
    auto main_layout = new QVBoxLayout(this);
    auto top_left_layout = new QVBoxLayout;
    auto top_right_layout = new QVBoxLayout;
    auto top_layout = new QHBoxLayout;
    auto descriptive_layout = new QFormLayout; // to add a descriptive text to the interval selector

    // displaying the layouts
    main_layout->addLayout(top_layout);
    top_layout->addLayout(top_left_layout);
    top_layout->addLayout(top_right_layout);
    top_right_layout->addLayout(descriptive_layout);

    // adding the widgets
    top_left_layout->addWidget(regex_input);
    top_left_layout->addWidget(source_input);
    top_left_layout->addWidget(destin_input);
    descriptive_layout->addRow("Check Folder Period:", interval_selector);
    top_right_layout->addWidget(run_button);
    top_right_layout->addWidget(interval_bar);
    main_layout->addWidget(graph);

    // assigning font
    QFont font ("Courier");
    regex_input->setFont(font);
    source_input->setFont(font);
    destin_input->setFont(font);

    // assigining placeholder texts and clear buttons
    regex_input->setPlaceholderText("Files REGEX");
    source_input->setPlaceholderText("Source Folder");
    destin_input->setPlaceholderText("Destination Folder");

    // assigning configuration file text
    regex_input->setText(backend->regex_str);
    source_input->setText(backend->src_dir_path);
    destin_input->setText(backend->dst_dir_path);

    // setting clear text button
    regex_input->setClearButtonEnabled(true);
    source_input->setClearButtonEnabled(true);
    destin_input->setClearButtonEnabled(true);

    // interval selector and timer configurations
    interval_selector->setRange(1, 0x7fff'ffff/1000); // max positive int that handles s to ms
    interval_selector->setSuffix("s");
    interval_selector->setValue(backend->run_interval);
    run_timer->setInterval(backend->run_interval * 1000); // from s to ms
    bar_timer->setInterval(200); // ms

    // load transfer statistics
    auto statistics = backend->read_statistics_file();
    graph->update_graph(statistics.data(), statistics.count());

    // bar that shows the interval remaining for next transfer
    interval_bar->setRange(0, run_timer->interval());
    interval_bar->setValue(run_timer->interval());
    interval_bar->setTextVisible(false);

    // signal-slot connections
    QObject::connect(run_button, &QPushButton::clicked, this, &MainWindow::run_button_pressed);
    QObject::connect(regex_input, &QLineEdit::editingFinished, this, &MainWindow::regex_input_changed);
    QObject::connect(source_input, &QLineEdit::editingFinished, this, &MainWindow::source_input_changed);
    QObject::connect(destin_input, &QLineEdit::editingFinished, this, &MainWindow::destin_input_changed);
    QObject::connect(interval_selector, &QSpinBox::editingFinished, this, &MainWindow::interval_selector_changed);
    QObject::connect(run_timer, &QTimer::timeout, this, &MainWindow::run_timer_alarmed);
    QObject::connect(bar_timer, &QTimer::timeout, this, &MainWindow::bar_timer_alarmed);
}


void MainWindow::run_button_pressed () {
    if (app_running) {
        // stop execution
        run_timer->stop();
        bar_timer->stop();
        interval_bar->setValue(run_timer->interval());
        run_button->setText("Run App");
        regex_input->setReadOnly(false);
        source_input->setReadOnly(false);
        destin_input->setReadOnly(false);
        interval_selector->setReadOnly(false);
    } else {
        // start execution
        run_button->setText("Stop App");
        regex_input->setReadOnly(true);
        source_input->setReadOnly(true);
        destin_input->setReadOnly(true);
        interval_selector->setReadOnly(true);
        // transfer files and update graph
        auto report = backend->transfer_files();
        graph->update_graph(&report);
        run_timer->start();
        bar_timer->start();
        interval_bar->setValue(0);
    }
    app_running = !app_running;
}

void MainWindow::regex_input_changed () {
    backend->regex_str = regex_input->text();
}

void MainWindow::source_input_changed () {
    backend->src_dir_path = source_input->text();
}

void MainWindow::destin_input_changed () {
    backend->dst_dir_path = destin_input->text();
}

void MainWindow::interval_selector_changed () {
    backend->run_interval = interval_selector->value();
    run_timer->setInterval(backend->run_interval * 1000);
    interval_bar->setRange(0, run_timer->interval());
    interval_bar->setValue(run_timer->interval());
}

void MainWindow::run_timer_alarmed () {
    auto report = backend->transfer_files();
    graph->update_graph(&report);
}

void MainWindow::bar_timer_alarmed () {
    interval_bar->setValue(run_timer->interval() - run_timer->remainingTime());
}


MainWindow::~MainWindow () {
    delete backend;
}
